/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ideiah.siteideiahjsp.dao;

import com.ideiah.siteideiahjsp.model.Trabalho;
import java.util.ArrayList;

/**
 *
 * @author Pedro
 */
public class TrabalhoDao extends Dao{

//<editor-fold defaultstate="collapsed" desc="Salvar">

    public boolean salvar(Trabalho trabalho) {
        return super.salvar(trabalho);
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Buscar">
    public ArrayList<Trabalho> buscar() {
        return (ArrayList<Trabalho>) buscarObjetos(Trabalho.class);
    }

    public Trabalho buscar(int codigo) {
        return (Trabalho) buscarObjeto(codigo, Trabalho.class);
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Deletar">
    public boolean deletar(int codigo) {
        return excluir(codigo, Trabalho.class);
    }
//</editor-fold>
}
